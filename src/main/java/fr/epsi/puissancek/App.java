package fr.epsi.puissancek;

import fr.epsi.puissancek.gui.MainFrame;

public class App {
    
    public static void main(String[] args) {
        MainFrame frame = new MainFrame();
        frame.askForNewGame();
    }
}
