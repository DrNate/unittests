package fr.epsi.puissancek.gui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import fr.epsi.puissancek.controller.GameController;

public class MainFrame {

    private final JFrame frame = new JFrame("Puissance K");
    private final JButton newGameButton = new JButton("Nouvelle partie");
    private final NewGameSettingsDialog newGameDialog = new NewGameSettingsDialog(frame);

    private final GamePanel gamePanel = new GamePanel();
    private final MovesPanel movesPanel = new MovesPanel(gamePanel);
    
    public MainFrame() {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(buildMainPanel());
        frame.setResizable(false);
    }
    
    public void askForNewGame() {
        newGameDialog.setVisible(true); // open settings dialog and wait for it to close
        
        if (newGameDialog.isOkOption()) {
            GameController controller = newGameDialog.buildGameController();
            gamePanel.newGame(controller);
            movesPanel.newGame(controller);
            
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        }
    }
    
    private JPanel buildMainPanel() {
        newGameButton.addActionListener((e) -> askForNewGame());
        
        JPanel controlPanel = new JPanel();
        controlPanel.add(newGameButton);
        
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(gamePanel, BorderLayout.CENTER);
        panel.add(movesPanel, BorderLayout.EAST);
        panel.add(controlPanel, BorderLayout.SOUTH);
        return panel;
    }
}
