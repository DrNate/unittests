package fr.epsi.puissancek.model;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import fr.epsi.puissancek.model.*;

class TestModel {
	
	//Tests : GameMoveList

	@Test
	void test_addMove_increment_list() {
		GameMoveList GML = new GameMoveList();
		GameMove testMove = new GameMove(PlayerColor.RED, 1);
		
		GML.addMove(testMove);		
		
		assertEquals(1, GML.getSize());
	}
	
	@Test
	void test_getAndRemoveLastMove(){
		GameMoveList GML = new GameMoveList();
		GameMove testMove = new GameMove(PlayerColor.RED, 1);
		
		GML.addMove(testMove);
		
		assertEquals(testMove, GML.getAndRemoveLastMove());
		assertEquals(0, GML.getSize());
		
	}

	@Test
	void test_getElementAt() {
		GameMoveList GML = new GameMoveList();
		GameMove testMove = new GameMove(PlayerColor.RED, 1);
		GameMove testMove2 = new GameMove(PlayerColor.YELLOW, 2);
		
		GML.addMove(testMove);
		GML.addMove(testMove2);
		
		assertEquals(testMove, GML.getElementAt(0));
		assertEquals(testMove2, GML.getElementAt(1));
	}
	
	
	//Tests : PlayerColor
	
	@Test
	void test_other() {
		assertEquals(PlayerColor.YELLOW, PlayerColor.RED.other());
	}
	
	//Tests : GameModel
	
	@Test
	void test_play() {
		GameModel GM = new GameModel(6, 1, 4);
		
		assert(GM.play(1, PlayerColor.RED));
		assert(!GM.play(7, PlayerColor.YELLOW));
		assert(!GM.play(1, PlayerColor.RED));
		assert(!GM.play(-1, PlayerColor.YELLOW));
	}
	
	@Test
	void test_getColumnHeight(){
		GameModel GM = new GameModel(6, 7, 4);
		
		GM.play(1, PlayerColor.RED);
		GM.play(1, PlayerColor.YELLOW);
		
		assertEquals(2, GM.getColumnHeight(1));
	}
	
	@Test
	void test_remove() {
		GameModel GM = new GameModel(6, 7, 4);
		
		GM.play(0, PlayerColor.RED);
		GM.play(0, PlayerColor.YELLOW);
		GM.play(0, PlayerColor.RED);
		GM.play(0, PlayerColor.YELLOW);
		GM.remove(0);
		
		assertEquals(3, GM.getColumnHeight(0));
		assertEquals(PlayerColor.RED, GM.getCell(0, 2));
		
		GM.remove(2);
		GM.remove(-1);
		GM.remove(7);
		
		for (int i = 0; i <= 5; i++) {
			for (int j = 0; j <= 6; j++) {
				if(i == 0 && j <= 2) {
					assertNotNull(GM.getCell(i, j));
				} else {
					assertNull(GM.getCell(i, j));
				}
			}
		}
		
		
	}
	
	
	@Test
	void test_calculateGameOver_and_isGameOver() {
		GameModel GM = new GameModel(2, 2, 3);
		
		GM.play(0, PlayerColor.RED);
		GM.play(0, PlayerColor.YELLOW);
		GM.play(1, PlayerColor.RED);
		
		assert(!GM.calculateGameOver());
		assert(!GM.isGameOver());
		
		GM.play(1, PlayerColor.YELLOW);
		
		assert(GM.calculateGameOver());
		assert(GM.isGameOver());
		
		GameModel GM_col_won = new GameModel(2, 2, 2);
		
		GM_col_won.play(1, PlayerColor.RED);
		GM_col_won.play(0, PlayerColor.YELLOW);
		GM_col_won.play(1, PlayerColor.RED);
		
		assert(GM_col_won.calculateGameOver());
		assert(GM_col_won.isGameOver());
		
		GameModel GM_diag1_won = new GameModel(2, 2, 2);
		
		GM_diag1_won.play(1, PlayerColor.RED);
		GM_diag1_won.play(0, PlayerColor.YELLOW);
		GM_diag1_won.play(0, PlayerColor.RED);
		
		assert(GM_diag1_won.calculateGameOver());
		assert(GM_diag1_won.isGameOver());
		
		GameModel GM_diag2_won = new GameModel(2, 2, 2);
		
		GM_diag2_won.play(0, PlayerColor.YELLOW);
		GM_diag2_won.play(1, PlayerColor.RED);
		GM_diag2_won.play(1, PlayerColor.YELLOW);
		
		assert(GM_diag2_won.calculateGameOver());
		assert(GM_diag2_won.isGameOver());
	}
	
	@Test
	void test_getWinner() {
		GameModel GM = new GameModel(2, 2, 2);
		
		assertNull(GM.getWinner());
		
		GM.play(0, PlayerColor.RED);
		GM.play(0, PlayerColor.YELLOW);
		GM.play(1, PlayerColor.RED);
		
		assertEquals(PlayerColor.RED, GM.getWinner());
		
		
	}
	
}
